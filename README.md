# Todo Application

## Objectives

Implement a React application that can create and list TODOs using AWS infrastructure.

## Success criteria

* Add TODOs without refreshing the page.
* Design the structure of a TODO
* Receive existing TODOs when refreshing the page.
* The items should be sorted by creation date.
